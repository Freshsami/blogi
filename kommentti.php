<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$paasivu = $_SERVER['PHP_SELF'];
include ('header.php'); ?>

<div class="container content">
    <?php
    try {
       
        $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
       
        $tietokanta->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        if ($_SERVER['REQUEST_METHOD'] === 'POST'){

            try {
                $teksti = filter_input(INPUT_POST, 'kommentti', FILTER_SANITIZE_STRING);
                $kirjoitus_id = filter_input(INPUT_POST, 'kirjoitus_id', FILTER_SANITIZE_NUMBER_INT);

                $kysely = $tietokanta->prepare("INSERT INTO kommentti(kayttaja_id, teksti, kirjoitus_id) VALUES (:kayttaja_id,:teksti,:kirjoitus_id)");

                $kysely->bindValue(':kayttaja_id', $_SESSION['kayttaja_id'], PDO::PARAM_INT);
                $kysely->bindValue(':teksti', $teksti, PDO::PARAM_STR);
                $kysely->bindValue(':kirjoitus_id', $kirjoitus_id, PDO::PARAM_INT);

                if ($kysely->execute()){
                    header("Location: kommentti.php?id=$kirjoitus_id");
                    exit;
                }else{
                    print '<p>';
                    print_r($tietokanta->errorInfo());
                    print '</p>';
                }
            } catch (PFOException $pdoex){
                print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage() . '</p>';
            }

        } else {
            $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
        }
        
        $sql = "SELECT *, kirjoitus.id as id
                FROM kirjoitus INNER JOIN kayttaja ON kirjoitus.kayttaja_id = kayttaja.id
                WHERE kirjoitus.id = '$id'
                ORDER BY paivays desc";
        
        $kysely = $tietokanta->query($sql);
        $tietue = $kysely->fetch();
        
        $paivays = date('d.m.Y G:i', strtotime($tietue['paivays']));
        $otsikko = $tietue['otsikko'];
        $teksti = $tietue['teksti'];
        $tunnus = $tietue['tunnus'];
        $id = $tietue['id'];
        print "<h2>" . $otsikko . "</h2>";
        print "<p>" . $paivays . " by " . $tunnus . "</p>";
        print "<p>" . $teksti . "</p>";

    ?>
    <p><strong>Kommentit</strong></p>
    <?php
        if (isset($_SESSION['kayttaja_id'])) {
    ?>
    <form id="lisaa_kommentti" method="post" action="<?php print($_SERVER['PHP_SELF']);?>">
        <input type="hidden" name="kirjoitus_id" value="<?php print $id;?>">
        <textarea name="kommentti" id="kommentti"></textarea>
        <br>
        <button>Tallenna</button>
    </form>
    <?php
        }
    ?>

    <?php
        //muodostetaan suoritettava sql-lause.
        $sql =  "SELECT teksti, kirjoitus_id as kirjoitus_id,kommentti.id as kommentti_id, kommentti.paivays as paivays, kayttaja.tunnus as tunnus
                FROM kommentti INNER JOIN kayttaja ON kayttaja.id = kommentti.kayttaja_id
                WHERE kommentti.kirjoitus_id = '$id'
                ";

        $kysely = $tietokanta->query($sql);
        if($kysely){ ?>
    <ul>
        <?php
            while ($tietue = $kysely->fetch()) {
                
                $paivays = date('d.m.Y G:i', strtotime($tietue['paivays']));
                $kayttaja_tunnus = $tietue['tunnus'];
                $kommentti = $tietue['teksti'];
                $kommentti_id = $tietue['kommentti_id'];
                $kirjoitus_id = $tietue['kirjoitus_id'];
                print "<li>" . $kommentti . " " . $paivays . " by " . $kayttaja_tunnus     . "\n";
                if (isset($_SESSION['kayttaja_id'])) {
                    print "<a onClick='return confirm(\"Poistetaanko varmasti kommentti?\")' href='poista_kommentti.php?id=$kommentti_id&kirjoitus_id=$kirjoitus_id'><span class='glyphicon glyphicon-trash'></span></a>";
                }
                print "</li>";
            }

        ?>
    </ul>
    <?php

        }

    } catch(PDOEXception $pdoex) {
        print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage() . '</p>';
    }
    ?>
</div>

<?php include ('footer.php'); ?>
