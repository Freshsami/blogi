<?php session_start();
session_regenerate_id(); 
?>

<!doctype html>

<html>
    <head>
        <title>Blogi</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Blogi</a>
                </div>
                <ul class="nav navbar-nav">
                    <li <?php if($paasivu ==="/blogi/index.php") echo 'class="active"'; ?> ><a href="index.php">Home</a></li>
                    <?php if (isset($_SESSION['kayttaja_id'])) {?>
                    <li <?php if($paasivu ==="/blogi/kirjoitus.php") echo 'class="active"'; ?> ><a href="kirjoitus.php">Lisää kirjoitus</a></li>

                    <li ><a href="ulos.php">Kirjaudu Ulos</a></li>

                    <?php }else{ ?>
                    <li <?php if($paasivu ==="/Blogi/kirjaudu.php") echo 'class="active"'; ?> ><a href="kirjaudu.php">Kirjaudu</a></li>
                    <?php    } ?>
                </ul>
            </div>
        </nav>