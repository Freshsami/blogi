<?php 
$paasivu = $_SERVER['PHP_SELF'];
include('header.php');
?>

<div class="container content">
    <?php

    if (isset($_SESSION['kayttaja_id'])){

        //avataan tietokantayhteys.
        $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
        //virhemoodi päälle
        $tietokanta->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

        if ($_SERVER['REQUEST_METHOD'] === 'POST'){
            try {

                $nimi = filter_input(INPUT_POST, 'nimi', FILTER_SANITIZE_STRING);
                $otsikko = filter_input(INPUT_POST, 'otsikko', FILTER_SANITIZE_STRING);
                $teksti = filter_input(INPUT_POST, 'teksti', FILTER_SANITIZE_STRING);    
                $kayttaja_id = $_SESSION['kayttaja_id'];  
                $kysely = $tietokanta->prepare("INSERT INTO kirjoitus(kayttaja_id, otsikko, teksti) VALUES (:kayttaja_id,:otsikko,:teksti)");

                $kysely->bindValue(':kayttaja_id', $kayttaja_id, PDO::PARAM_INT);
                $kysely->bindValue(':otsikko', $otsikko, PDO::PARAM_STR);
                $kysely->bindValue(':teksti', $teksti, PDO::PARAM_STR);

                if ($kysely->execute()){
                    print '<p>Kirjoitus tallennettu</p>';
                }else{
                    print '<p>';
                    print_r($tietokanta->errorInfo());
                    print '</p>';
                }

            } catch (PFOException $pdoex){
                print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage() . '</p>';
            }
        }

    ?>
    <h1>Lisää kirjoitus</h1>


    <form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
        <div class="form-group">
            <label for="otsikko">Otsikko</label>
            <input class="form-control" id="otsikko" name='otsikko' type='text'> 
        </div>
        <div class="form-group">
            <label for="teksti">Teksti</label>
            <textarea class="form-control" id="teksti" name="teksti"></textarea>
        </div>

        <input class="btn btn-primary" type="submit" value="Tallenna" />
        <input class="btn btn-default" type="reset" value="Peruuta" />
    </form> 
    <?php } else { ?>

    <p>Et ole kirjautuneena!</p>

    <?php } ?>
</div>
<?php  include('footer.php');?>