<?php
$paasivu = $_SERVER['PHP_SELF'];
include('header.php'); 
?>

<div class="container">
    <?php
    try{
        $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
        $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO:: ERRMODE_EXCEPTION);
        
        $sql = "SELECT *,kirjoitus.id as id FROM kirjoitus INNER JOIN kayttaja ON kirjoitus.kayttaja_id = kayttaja.id" . " ORDER BY paivays desc";

        $kysely = $tietokanta->query($sql);

        if($kysely){
            while($tietue = $kysely->fetch()){
                $paivays = date('d.m.Y G:i', strtotime($tietue['paivays'])); 
                $otsikko = $tietue['otsikko'];
                $teksti = $tietue['teksti'];
                $tunnus = $tietue['tunnus'];
                $id = $tietue['id'];
                print "<p>" . $paivays . " by " . $tunnus . "</p>";
                print "<p><a href='kommentti.php?id=" . $id . "'>" . $otsikko . "</a></p>";
                
                if (isset($_SESSION['kayttaja_id'])){
                    print "<a onClick='return confirm(\"Poistetaanko varmasti kirjoitus?\")'  href='poista.php?id=" . $tietue['id'] . " ' ><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a>";
                } 
                print "<hr>";
            }
        }
        else{
            print '<p>';
            print_r($tietokanta->errorInfo());
            print '</p>';
        }
    }
    catch(PDOException $pdoex){
        print '<p>Tietokannan avaus epäonnistui ' . $pdoex->getMessage() . '</p>';
    }
    ?>
</div>

<?php include('footer.php'); ?>