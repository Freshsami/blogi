<?php 
$paasivu = $_SERVER['PHP_SELF'];
include('header.php');

?>
<div class="container content">
<?php
$viesti = "";

$tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');

$tietokanta->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    if($tietokanta != null){
        try{
            $tunnus = filter_input(INPUT_POST, 'tunnus', FILTER_SANITIZE_STRING);
            $salasana = md5(filter_input(INPUT_POST, 'salasana', FILTER_SANITIZE_STRING));

            $sql = "SELECT * FROM kayttaja WHERE tunnus='$tunnus' AND salasana='$salasana'";
            $kysely = $tietokanta->query($sql);

            if ($kysely->rowCount()===1) {
                $tietue = $kysely->fetch();
                $_SESSION['login'] = true;
                $_SESSION['kayttaja_id'] = $tietue['id'];
                header('Location: index.php');
            } else {
                $viesti = "Väärä tunnus tai salasana!";
            }
        } catch(PDOEXception $pdoex) {
            print '<p>Käyttäjän tietojen hakeminen epäonnistui. ' . $pdoex->getMessage() . '</p>';
        }
        print $viesti;
    }
}
?>

<div class="container">
    <form method="post" action="<?php print $_SERVER['PHP_SELF']; ?>">
        <div class="form-group">
            <label>Tunnus:</label>
            <input type="text" class="form-control" name="tunnus">
        </div>
        <div class="form-group">
            <label>Salasana:</label>
            <input type="password" class="form-control" name="salasana">
        </div>
        <button type="submit" class="btn btn-default">Kirjaudu</button>
    </form>
</div>
</div>
<?php include('footer.php'); ?>



