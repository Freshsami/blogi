<?php
$paasivu = $_SERVER['PHP_SELF'];
include('header.php'); ?>
<div class="container">
    <?php
    if($_SERVER['REQUEST_METHOD'] === 'GET'){
        if(isset($_GET['id'])){
            $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
            
            try{
                $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');

                $tietokanta->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

                $sql = "DELETE
            FROM kirjoitus 
            WHERE id = '$id'";

                $kysely = $tietokanta->query($sql);

                if($kysely){
                    print "<p>Kirjoitus poistettu.</p>";
                    print "<a href='index.php'>Takaisin etusivulle</a>";
                }
            }
            catch(PDOException $pdoex){
                print '<p>Tietokannan avaus epäonnistui ' . $pdoex->getMessage() . '</p>';
            }
        }}
    ?> 
</div>

<?php include('footer.php'); ?>